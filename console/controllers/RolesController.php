<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 11.12.17
 * Time: 20:48
 */


namespace console\controllers;

use Yii;
use yii\console\Controller;
/**
 * Инициализатор RBAC выполняется в консоли php yii roles/init
 */
class RolesController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        //Создаем пермишены
        $permissionDeleteUser = $auth->createPermission('deleteUser');
        $permissionSwitchUser = $auth->createPermission('SwitchUser');
        $permissionSwitchUser->description = 'Разрешение на переключение';
        $permissionDeleteUser->description = 'Разрешение на удаление пользователя';
        $auth->add($permissionDeleteUser);
        $auth->add($permissionSwitchUser);

        //Создаем роль админа
        $admin = $auth->createRole('admin');
        $auth->add($admin);

        //Создаем роль менеджера
        $manager = $auth->createRole('manager');
        $auth->add($manager);
        $auth->addChild($manager, $permissionDeleteUser);

        //Назначаем роль админа пользователю с id 1
        $auth->assign($admin, 8);
        //Назначаем роль менеджера пользователю с id 2
        $auth->assign($manager, 9php);





    }
}



