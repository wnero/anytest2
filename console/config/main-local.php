<?php
return [
    'bootstrap' => ['gii'],
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],




    'components' => [



        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'transport' => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => 'smtp.mailtrap.io',
                'username'   => 'b6aac36ca63819',
                'password'   => 'fdb78f796dbe03', // your password
                'port'       => '465',
              'encryption' => 'tls',
            ],



            'useFileTransport' => false,
        ],
    ],





];
