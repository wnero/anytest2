<?php

/* @var $this yii\web\View */


use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Админ часть сайта</h1>


        <a class="btn btn-warning" href="http://localhost/backend/web/index.php?r=/user/admin/index">Все пользователи</a>


        <br>
        <p>А тут нужно сгенерировать ссылку на http://localhost/frontend/web/index.php?r=site%2Fcontact </p><br>
        c  помощью  Url::to .

        первый способ получить ссылку

        <?= Html::a('Ссылка на пользввателей в дектрум', ['/user/admin/index'], ['clsss' => 'btn btn-success']) ?>

        второй способ получить ссылку

        <a href="<?= Url::to(['/user/admin/index']) ?>" class="btn btn-primary">Вторая ссылка на пользователей в дектрум</a>


    </div>

    </div>