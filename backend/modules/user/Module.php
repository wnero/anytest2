<?php

namespace backend\modules\user;

class Module extends \dektrium\user\Module
{
    public $controllerMap = [
        'admin'    => 'backend\controllers\AdminController',
                       //  тут обращение не по пути файла по неймспейсу файла))
        'security' => 'backend\controllers\SecurityController',

        'registration'=>'backend\controllers\RegistrationController',

       // 'role' => 'backend\rbac\controllers\RoleController',


    ];
}
