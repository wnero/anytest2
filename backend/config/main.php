<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],






    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
//        'user' => [
//            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => true,
//            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
//        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
           'enablePrettyUrl' => false,
            'showScriptName' => false,
        ],



        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
        ],


        'view'=>[
            'theme'=>[
                'pathMap'=>[
                    '@dektrium/user/views'=>'backend/views/'
                ],
            ],
        ],

        ],






    'modules' => [

//        'user' => [
//
//
////
////            // following line will restrict access to profile, recovery, registration and settings controllers from backend
////            'as backend' => 'dektrium\user\filters\BackendFilter',
////          // 'class'  => 'dektrium\user\Module',
////            'class'    => 'backend\modules\user\Module',
////            'viewPath' => '@backend/views/',
////            'admins' => ['url'],
////
////        ],



        'user' => [
           // 'class'  => 'dektrium\user\Module',
            'class'    => 'backend\modules\user\Module',
            'viewPath' => '@backend/views',
            'admins'   => ['url'],
        ],







//        'rbac'=>[
//            'viewPath'=>'@backend/views',
//
//        ],


       // 'rbac' => 'backend\modules\user\Module',

       'rbac' => 'dektrium\rbac\RbacWebModule',

    ],










    'params' => $params,
];
