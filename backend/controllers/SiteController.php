<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'test','mypage'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['/user/admin/index']);


       // return $this->render('index');

        //  Тут нужно сделать редирект на екшн  actionIndex()

        // Который лежит по юрлу /var/www/html/vendor/dektrium/yii2-user/controllers/AdminController.php


    }

    public function actionTest()
    {
        Yii::$app->session->setFlash('success', 'Редирект с site/test');
        //редирект в контроллер дектрума
        //user - название компоненты (или модуля)
        //admin/index - контроллер и экшен
        //return $this->redirect(['/user/admin/index']);

        //редирект внутри контроллера на другой экшен
        return $this->redirect(['test-two']);

    }

    public function actionTestTwo()
    {
        var_dump('test');
    }




    public function actionMypage()
    {

        return $this->render('mypage');




    }






    public function actionHello()
    {

       echo 'Hello';




    }









    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
