<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 06.02.18
 * Time: 19:03
 */


use yii\widgets\Pjax;
use yii\helpers\Html; // Для создания кнопки

$this->title = 'Cтраница магазина';


?>



<h1>Cтраница магазина</h1>


<?php


//foreach ($shopbd as $current) {
//    echo $current['name'];
//    echo $current['price'];
//    echo $current['date'];
//
//}

?>



<?php Pjax::begin(); ?>







<div id="sidebar-left" class="col-sm-2 sidebar left_content">
  <h5> <i>Категории товаров </i></h5>



    <?= Html::beginForm(['site/tvset'], 'get', ['data-pjax' => '', 'class' => '']); ?>

    <?= Html::a('Телевизоры', ['site/tvset'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\tvset' ] ],'class'=>'c_names' ])?>

    <?= Html::endForm() ?>


    <?= Html::beginForm(['site/freeze'], 'get', ['data-pjax' => '', 'class' => '']); ?>
       <?= Html::a('Холодильники', ['site/freeze'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\freeze' ] ],'class'=>'c_names' ])?>

    <?= Html::endForm() ?>


    <?= Html::beginForm(['site/notes'], 'get', ['data-pjax' => '', 'class' => '']); ?>

    <?= Html::a('Ноутбуки', ['site/notes'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\notes' ] ],'class'=>'c_names' ])?>

    <?= Html::endForm() ?>




    <?= Html::beginForm(['site/shop'], 'post', ['data-pjax' => '', 'class' => '']); ?>

    <?= Html::a('ВСЕ ТОВАРЫ', ['site/shop'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\shop' ] ],'class'=>'c_names' ])?>

    <?= Html::endForm() ?>













</div>





<!--      СОРТИРОВКА    -->








<div class="dropdown drodowngoods">
    <a class="dropdown-toggle" type="button" data-toggle="dropdown">Сортировка товаров</a>
    <ul class="dropdown-menu">






        <!--  ;;;;;;; ОБЩАЯ  СОРТИРОВКА  ;;;;;;;;;;;;; -->

        <!--    CОРТИРОВКА ПО ЦЕНЕ  -->




        <?php if($id=='shop'):   ?>

            <?= Html::beginForm(['site/sortbycheapshop'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('Дешевые', ['site/sortbycheapshop'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbycheapshop' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>

        <!--    CОРТИРОВКА ПО АЛФАВИТУ (ОТ А ДО Я)-->


        <?php if($id=='shop'):   ?>

            <?= Html::beginForm(['site/sortbyabcshop'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('По алфавиту (А-Z)', ['site/sortbyabcshop'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbyabcshop' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>

        <!--    CОРТИРОВКА / НОВЫЕ /  -->


        <?php if($id=='shop'):   ?>

            <?= Html::beginForm(['site/sortbynewshop'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('Новые', ['site/sortbynewshop'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbynewshop' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>
































        <!--  ;;;;;;;СОРТИРОВКА ДЛЯ ТЕЛЕВИЗОРОВ  ;;;;;;;;;;;;; -->

        <!--    CОРТИРОВКА ПО ЦЕНЕ  -->


        <?php if($id=='tvset'):   ?>

            <?= Html::beginForm(['site/sortbycheaptvset'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('Дешевые', ['site/sortbycheaptvset'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbycheaptvset' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>

        <!--    CОРТИРОВКА ПО АЛФАВИТУ (ОТ А ДО Я)-->


        <?php if($id=='tvset'):   ?>

            <?= Html::beginForm(['site/sortbyabctvset'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('По алфавиту (А-Z)', ['site/sortbyabctvset'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbyabctvset' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>

        <!--    CОРТИРОВКА / НОВЫЕ /  -->


        <?php if($id=='tvset'):   ?>

            <?= Html::beginForm(['site/sortbynewtvset'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('Новые', ['site/sortbynewtvset'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbynewtvset' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>

















        <!--  ;;;;;;;СОРТИРОВКА ДЛЯ ХОЛОДИЛЬНИКОВ ;;;;;;;;;;;;; -->

        <!--    CОРТИРОВКА ПО ЦЕНЕ  -->


        <?php if($id=='freeze'):   ?>

            <?= Html::beginForm(['site/sortbycheapfreeze'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('Дешевые', ['site/sortbycheapfreeze'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbycheapfreeze' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>

        <!--    CОРТИРОВКА ПО АЛФАВИТУ (ОТ А ДО Я)-->


        <?php if($id=='freeze'):   ?>

            <?= Html::beginForm(['site/sortbyabcfreeze'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('По алфавиту (А-Z)', ['site/sortbyabcfreeze'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbyabcfreeze' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>

       <!--    CОРТИРОВКА / НОВЫЕ /  -->


        <?php if($id=='freeze'):   ?>

            <?= Html::beginForm(['site/sortbynewfreeze'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('Новые', ['site/sortbynewfreeze'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbynewfreeze' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>















          <!-- ;;;;; СОРТИРОВКА ДЛЯ НОУТБУКОВ;;;;;;;;-->

        <!--    CОРТИРОВКА ПО ЦЕНЕ--дешевка-->


        <?php if($id=='notes'):   ?>

            <?= Html::beginForm(['site/sortbycheapnotes'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('Дешевые', ['site/sortbycheapnotes'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbycheapnotes' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>


        <!--    CОРТИРОВКА ПО АЛФАВИТУ (ОТ А ДО Я)-->


        <?php if($id=='notes'):   ?>

        <?= Html::beginForm(['site/sortbyabcnote'], 'post', ['data-pjax' => '', 'class' => '']); ?>

        <li class="droplist"> <?= Html::a('По алфавиту (А-Z)', ['site/sortbyabcnote'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbyabcnote' ] ],'class'=>'droplist' ])?></li>

        <?= Html::endForm() ?>


        <?php endif;  ?>


        <!--    CОРТИРОВКА / НОВЫЕ /  -->


        <?php if($id=='notes'):   ?>

            <?= Html::beginForm(['site/sortbynewnotes'], 'post', ['data-pjax' => '', 'class' => '']); ?>

            <li class="droplist"> <?= Html::a('Новые', ['site/sortbynewnotes'], ['data' => ['method' => 'post','params' => [ 'action' => 'site\sortbynewnotes' ] ],'class'=>'droplist' ])?></li>

            <?= Html::endForm() ?>


        <?php endif;  ?>
















    </ul>
</div>




<!-- ;;;;;;;;;;;;;; КОНЕЦ СОРТИРОВКИ ;;;;;;;;;;;;;;;;;;;;;;;;; ->






<?php

//var_dump($shopbd2);

//foreach ($shopbd2 as $current) {
//    echo $current['name'];
//    echo $current['price'];
//    echo $current['date'];
//
//}
//


?>



























<!--   ;;;;;;;;;;;;;;  ТОВАРЫ    ;;;;;;;;;;;;;      -->




<div id="goodslist" class="row row_catalog_products ">



    <?php




//    var_dump($shopbd2);
//
//    echo 'End of ends';
//
//    die();

    ?>


    <?php foreach ($shopbd2  as $current): ?>





        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col_product">


            <div class="product">

                <div class="wrapper_product_hover">
                    <div class="product_hover">
                        <div class="p_description">

                            <p><?php   echo $current['name'];   ?></p>


                            <div class="new_old_price clearfix">
                                <p class="price"><span class="new_price"><span class="ccp">Цена </span><?php  echo $current['price']; ?> </span></p>

                     <button  class="btn btn-primary btnmain" type="submit" data-toggle="modal" data-target="<?= '#'.$current['id'];?>" >Купить</button>




                            </div>

                            <table class="listing_attrs"><tbody><tr><td>Дата поставки:<?php   echo $current['date']; ?></td></tr></tbody></table>

                        </div>
                    </div>
                </div>
            </div>




        </div>



        <!--  МОДАЛЬНОЕ ОКНО-->


        <div id="" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
                        <h4 class="modal-title"><?php echo 'Купить'.$current['name'];  ?></h4>
                    </div>
                    <div class="modal-body">
                        <ul>
                            <li>Цена: <?php  echo $current['price']; ?>  </li>
                            <li>Дата поставки:<?php   echo $current['date']; ?> </li>

                        </ul>


                    </div>
                    <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button></div>
                </div>
            </div>
        </div>


        <!--       КОНЕЦ   МОДАЛЬНОГО ОКНА-->









    <?php endforeach; ?>





    <?php



//    foreach ($modx as $current) {
//        echo $current['name'];
//        echo $current['price'];
//        echo $current['date'];
//
//    }
//


    ?>


















</div>






<?php


// echo 'Айдибатон'.$id;





?>




<?php Pjax::end(); ?>


<button class="btnmain">fff</button>

<script>


    var btn=parentDOM.getElementsByClassName('btnmain');


    btn.onclick=function(){


        alert("Hello user 555 !!");

    };




</script>
