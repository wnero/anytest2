<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Goods;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }




    //далее для страницы пробного магазина

    public function actionShop()
    {



        $shopbd2=Goods::find()->all();


        $id='shop';




      //  return $this->render('shop', ['shopbd2' => $shopbd2, 'id' => $id, 'modx' => $modx]);

       return $this->render('shop',compact('shopbd2','id')); // тут тлько для двух перемнных передача


    }




//    public function actionGoods()
//    {
//
//
//        $gid=$_POST['gid'];
//
//        $shopbd2=$_POST['curntshop'];
//
//        $id=$_POST['retid'];
//
//        $modx=Goods::find()->asArray()->where(['=','category_id',$gid])->all();
//
//
//
//
//
//
//         return $this->render('shop', [ 'shopbd2'=>$shopbd2,'modx' => $modx,'gid'=>$gid,'id'=>$id]);
//
//
//    }
//
//






























    public function actionTvset()
    {

   //  Показать товары из категории Телевизоры



        $shopbd2=Goods::find()->asArray()->where(['=','category_id','0'])->all();

//        $idbutton=$_GET['id'];

        $id='tvset';


        return $this->render('shop',compact('shopbd2','id'));
    }




    public function actionNotes()
    {

        //  Показать товары из категории Ноутбуки



        $shopbd2=Goods::find()->asArray()->where(['=','category_id','2'])->all();

        $id='notes';


        return $this->render('shop',compact('shopbd2','id'));
    }



    public function actionFreeze()
    {

        //  Показать товары из категории Холодильники



        $shopbd2=Goods::find()->asArray()->where(['=','category_id','1'])->all();


        $id='freeze';

        return $this->render('shop',compact('shopbd2','id'));
    }
































          //;;;;;;;;;;;;  ДАЛЕЕ СОРТИРОВКА ТОВАРОВ ;;;;;;;;;;;;;


    //;;;;;;;;;;;;  ДАЛЕЕ СОРТИРОВКА ТОВАРОВ ;;;;;;;;;;;;;


//    СОРТИРОВКА ПО ЦЕНЕ
    public function actionSortbycheapshop()
    {

        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.от большего к меньшему



        $shopbd2=Goods::find()->addOrderBy(['price' => SORT_ASC])->all();

        $id='shop';



        return $this->render('shop',compact('shopbd2','id'));


    }

    //    СОРТИРОВКА ПО АЛФАВИТУ
    public function actionSortbyabcshop()
    {
        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.от большего к меньшему


        $shopbd2=Goods::find()->asArray()->addOrderBy(['name' => SORT_ASC])->all();

        $id='shop';



        return $this->render('shop',compact('shopbd2','id'));


    }






    public function actionSortbynewshop()
    {




        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.от большего к меньшему


        $shopbd2=Goods::find()->addOrderBy(['date' =>SORT_DESC])->all();

        $id='shop';



        return $this->render('shop',compact('shopbd2','id'));


    }











//    ;;;;;;;;;;; СОРТИРОВКА ТЕЛЕВИЗОРОВ ;;;;;;;;;;;;;;;;;


   // ASC сортирует от меньшего к большему, а DESC от большего к меньшему.


//    СОРТИРОВКА ПО ЦЕНЕ
   public function actionSortbycheaptvset()
    {

        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.DESC от большего к меньшему.



        $shopbd2=Goods::find()->asArray()->where(['=','category_id','0'])->addOrderBy(['price' => SORT_ASC])->all();

        $id='tvset';



        return $this->render('shop',compact('shopbd2','id'));


    }

    //    СОРТИРОВКА ПО АЛФАВИТУ
    public function actionSortbyabctvset()
    {
        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.от большего к меньшему


        $shopbd2=Goods::find()->asArray()->where(['=','category_id','0'])->addOrderBy(['name' => SORT_ASC])->all();

        $id='tvset';



        return $this->render('shop',compact('shopbd2','id'));


    }






    public function actionSortbynewtvset()
    {



         // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.от большего к меньшему


        $shopbd2=Goods::find()->asArray()->where(['=','category_id','0'])->addOrderBy(['date' =>SORT_DESC])->all();

        $id='tvset';



        return $this->render('shop',compact('shopbd2','id'));


    }










//    ;;;;;;;;;  СОРТИРОВКА ХОЛОДИЛЬНИКОВ;;;;;;;;;;;


//    СОРТИРОВКА ПО ЦЕНЕ

    public function actionSortbycheapfreeze()
    {



        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.от большего к меньшему


        $shopbd2=Goods::find()->asArray()->where(['=','category_id','1'])->addOrderBy(['price' => SORT_ASC])->all();

        $id='freeze';



        return $this->render('shop',compact('shopbd2','id'));


    }




   //    СОРТИРОВКА ПО АЛФАВИТУ


    public function actionSortbyabcfreeze()
    {




        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.от большего к меньшему



        $shopbd2=Goods::find()->asArray()->where(['=','category_id','1'])->addOrderBy(['name' => SORT_ASC])->all();

        $id='freeze';



        return $this->render('shop',compact('shopbd2','id'));


    }






    public function actionSortbynewfreeze()
    {



        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.от большего к меньшему

        $shopbd2=Goods::find()->asArray()->where(['=','category_id','1'])->addOrderBy(['date' =>SORT_DESC])->all();

        $id='freeze';



        return $this->render('shop',compact('shopbd2','id'));


    }







//      ;;;;;;;;;;;;;;;     СОРТИРОВКА НОУТБУКОВ;;;;;;;;;;;;;;;;;;;;



    public function actionSortbyabcnote()
    {




        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.от большего к меньшему

        $shopbd2=Goods::find()->asArray()->where(['=','category_id','2'])->addOrderBy(['name' => SORT_ASC])->all();

        $id='notes';



        return $this->render('shop',compact('shopbd2','id'));


    }



// СОРТИРОВКА ПО ЦЕНЕ
    public function actionSortbycheapnotes()
    {
        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.от меньшего к большему
        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.от большего к меньшему

        $shopbd2=Goods::find()->asArray()->where(['=','category_id','2'])->addOrderBy(['price' => SORT_ASC])->all();

        $id='notes';



        return $this->render('shop',compact('shopbd2','id'));


    }







// СОРТИРОВКА -- НОВЫЕ
    public function actionSortbynewnotes()
    {

        // SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.


        // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.

        $shopbd2=Goods::find()->asArray()->where(['=','category_id','2'])->addOrderBy(['date' =>SORT_DESC ])->all();

        $id='notes';



        return $this->render('shop',compact('shopbd2','id'));


    }









































































    public function actionTest()
    {


// SORT_ASC используется с array_multisort() для сортировки в порядке возрастания.


      // SORT_DESC используется с array_multisort() для сортировки в порядке убывания.

       $shopbd2=Goods::find()->asArray()->where(['=','category_id','2'])->addOrderBy(['name' => SORT_ASC])->all();





        return $this->render('test',compact('shopbd2'));




    }



    public function actionTest2()
    {



        $shopbd2=Goods::getNotes();









        return $this->render('test',compact('shopbd2'));





    }



//The end actions for shop







    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
